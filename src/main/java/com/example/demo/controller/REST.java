package com.example.demo.controller;

//Framework which allows REST to be implemented 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.controller.Details.SampleDetails;


@RestController
@ResponseStatus

///this class will handle all the REST  e.g. GET,POST etc 
public class REST {
	
	// auto wired was used as it allowed 
	@Autowired
	private SampleDetails sampleDetails;
	
	// using the GET or Request REST method for this API sample, using the GET operate to receive the data 
	// listed below
	@RequestMapping (value= "/getUserDetails", method=RequestMethod.GET)
	public SampleDetails getUserDetails() {
		
		sampleDetails.setID("1");
		sampleDetails.setName("Harveer Singh Jandu");
		sampleDetails.setAge("22");
		sampleDetails.setGender("Male");
		
		//if (getUserDetails == null) throw new 
		
		return sampleDetails;
		 
		//@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such Order")  // 404 
		
	}
	
	
}
 